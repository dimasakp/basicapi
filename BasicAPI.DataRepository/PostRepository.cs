﻿using BasicAPI.BusinessEntity;
using BasicAPI.BusinessEntity.Parameter;
using BasicAPI.DataRepository.Interface;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Transactions;

namespace BasicAPI.DataRepository
{
    public class PostRepository : IPostRepository
    {
        #region Variables

        string ConnectionString;

        private readonly IConfiguration _configuration;

        #endregion

        #region Constructor

        public PostRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        #endregion

        public bool Delete(int PostId)
        {
            try
            {
                Get(PostId);

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (var transaction = new TransactionScope())
                    {
                        string sql0 = @"DELETE FROM [Post1] WHERE PostId = @PostId;
                            DELETE FROM [Post0] WHERE PostId = @PostId";

                        try
                        {
                            connection.Execute(sql0, new { PostId = PostId });

                            transaction.Complete();
                        }
                        catch (Exception ex)
                        {
                            transaction.Dispose();
                            throw ex;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Post0ResponseEntity> Get()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<Post0ResponseEntity> post0 = new List<Post0ResponseEntity>();
                    List<Post1ResponseEntity> post1 = new List<Post1ResponseEntity>();

                    string sql0 = @"SELECT * FROM [Post0]";
                    string sql1 = @"SELECT A.[TagId], [Label] 
                            FROM [Post1] A
                            INNER JOIN [Tag0] B ON
                            B.[TagId] = A.[TagId]
                            WHERE [PostId] = @PostId";

                    var _post0 = connection.Query<Post0ResponseEntity>(sql0);
                    post0 = _post0.ToList();

                    foreach (Post0ResponseEntity x in post0)
                    {
                        var _post1 = connection.Query<Post1ResponseEntity>(sql1, x);
                        post1 = _post1.ToList();

                        x.Tags = new List<Post1ResponseEntity>();
                        x.Tags.AddRange(post1);
                    }

                    return post0;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public Post0ResponseEntity Get(int PostId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    Post0ResponseEntity post0 = new Post0ResponseEntity();
                    List<Post1ResponseEntity> post1 = new List<Post1ResponseEntity>();

                    string sql0 = @"SELECT * FROM [Post0] WHERE [PostId] = @PostId";
                    string sql1 = @"SELECT A.[TagId], [Label] 
                            FROM [Post1] A
                            INNER JOIN [Tag0] B ON
                            B.[TagId] = A.[TagId]
                            WHERE [PostId] = @PostId";

                    var _post0 = connection.Query<Post0ResponseEntity>(sql0, new { PostId = PostId });
                    post0 = _post0.FirstOrDefault();

                    if (post0 == null)
                    {
                        throw new Exception("Data not found");
                    }

                    var _post1 = connection.Query<Post1ResponseEntity>(sql1, new { PostId = PostId });
                    post1 = _post1.ToList();

                    post0.Tags = new List<Post1ResponseEntity>();
                    post0.Tags.AddRange(post1);

                    return post0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Insert(Post0ParameterEntity Post)
        {
            try
            {
                int postId;

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (var transaction = new TransactionScope())
                    {
                        string sql0 = @"INSERT INTO [Post0] VALUES 
                                (@Title, @Content)
                                SELECT SCOPE_IDENTITY()";
                        string sql1 = @"INSERT INTO [Post1] VALUES 
                                (@PostId, @TagId)
                                ";

                        try
                        {
                            var _postId = connection.Query<int>(sql0, Post);
                            postId = _postId.First();

                            if (Post.Tags.Count > 0)
                            {
                                List<Post1Entity> post1 = new List<Post1Entity>();

                                foreach (Post1ParameterEntity x in Post.Tags)
                                {
                                    post1.Add(new Post1Entity
                                    {
                                        PostId = postId,
                                        TagId = x.TagId,
                                    });
                                }

                                connection.Execute(sql1, post1);
                            }

                            transaction.Complete();
                        }
                        catch (Exception ex)
                        {
                            transaction.Dispose();
                            throw ex;
                        }
                    }

                    return postId;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Update(Post0ParameterEntity Post, int PostId)
        {
            try
            {
                Post0ResponseEntity post0 = Get(PostId);
                List<int> _addTags = new List<int>();
                List<int> _deleteTags = new List<int>();
                List<int> oldTags = new List<int>();
                List<int> newTags = new List<int>();

                post0.Tags.ForEach(x => oldTags.Add(x.TagId ));
                Post.Tags.ForEach(x => newTags.Add(x.TagId ));

                _addTags = newTags.Except(oldTags).ToList();
                _deleteTags = oldTags.Except(newTags).ToList();

                List<Post1Entity> addTags = new List<Post1Entity>();
                List<Post1Entity> deleteTags = new List<Post1Entity>();

                _addTags.ForEach(x => addTags.Add(new Post1Entity { PostId = PostId, TagId = x }));
                _deleteTags.ForEach(x => deleteTags.Add(new Post1Entity { PostId = PostId, TagId = x }));
                
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {

                    using (var transaction = new TransactionScope())
                    {
                        string sql0 = @"UPDATE [Post0] SET 
                                Title = @Title,
                                Content = @Content
                                WHERE PostId = @PostId
                                ";
                        string sql1 = @"INSERT INTO [Post1] VALUES 
                                (@PostId, @TagId)
                                ";
                        string sql2 = @"DELETE [Post1] WHERE
                                PostId = @PostId AND TagId = @TagId
                                ";

                        try
                        {
                            connection.Execute(sql0, new { PostId = PostId, Title = Post.Title, Content = Post.Content });
                            connection.Execute(sql1, addTags);
                            connection.Execute(sql2, deleteTags);

                            transaction.Complete();
                        }
                        catch (Exception ex)
                        {
                            transaction.Dispose();
                            throw ex;
                        }
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
