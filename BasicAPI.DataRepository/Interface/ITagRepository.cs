﻿using BasicAPI.BusinessEntity;
using BasicAPI.BusinessEntity.Parameter;
using System;
using System.Collections.Generic;
using System.Text;

namespace BasicAPI.DataRepository.Interface
{
    public interface ITagRepository
    {
        public List<Tag0ResponseEntity> Get();
        public Tag0ResponseEntity Get(int TagId);
        public List<int> Insert(List<Tag0ParameterEntity> Tag);
        public bool Update(Tag0ParameterEntity Tag, int TagId);
        public bool Delete(int TagId);
    }
}
