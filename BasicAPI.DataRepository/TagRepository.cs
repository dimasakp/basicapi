﻿using BasicAPI.BusinessEntity;
using BasicAPI.BusinessEntity.Parameter;
using BasicAPI.DataRepository.Interface;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Transactions;

namespace BasicAPI.DataRepository
{
    public class TagRepository : ITagRepository
    {
        #region Variables

        string ConnectionString;

        private readonly IConfiguration _configuration;

        #endregion

        #region Constructor

        public TagRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        #endregion

        public bool Delete(int TagId)
        {
            try
            {
                Get(TagId);

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (var transaction = new TransactionScope())
                    {
                        string sql0 = @"DELETE FROM [Post1] WHERE TagId = @TagId;
                            DELETE FROM [Tag0] WHERE TagId = @TagId";

                        try
                        {
                            connection.Execute(sql0, new { TagId = TagId });

                            transaction.Complete();
                        }
                        catch (Exception ex)
                        {
                            transaction.Dispose();
                            throw ex;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Tag0ResponseEntity> Get()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    List<Tag0ResponseEntity> tag0 = new List<Tag0ResponseEntity>();
                    List<Tag1ResponseEntity> tag1 = new List<Tag1ResponseEntity>();

                    string sql0 = @"SELECT * FROM [Tag0]";
                    string sql1 = @"SELECT A.[PostId], [Title], [Content] 
                            FROM [Post1] A
                            INNER JOIN [Post0] B ON
                            B.[PostId] = A.[PostId]
                            WHERE [TagId] = @TagId";

                    var _tag0 = connection.Query<Tag0ResponseEntity>(sql0);
                    tag0 = _tag0.ToList();

                    foreach (Tag0ResponseEntity x in tag0)
                    {
                        var _tag1 = connection.Query<Tag1ResponseEntity>(sql1, x);
                        tag1 = _tag1.ToList();

                        x.Posts = new List<Tag1ResponseEntity>();
                        x.Posts.AddRange(tag1);
                    }

                    return tag0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Tag0ResponseEntity Get(int TagId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    Tag0ResponseEntity tag0 = new Tag0ResponseEntity();
                    List<Tag1ResponseEntity> tag1 = new List<Tag1ResponseEntity>();

                    string sql0 = @"SELECT * FROM [Tag0] WHERE [TagId] = @TagId";
                    string sql1 = @"SELECT A.[PostId], [Title], [Content] 
                            FROM [Post1] A
                            INNER JOIN [Post0] B ON
                            B.[PostId] = A.[PostId]
                            WHERE [TagId] = @TagId";

                    var _tag0 = connection.Query<Tag0ResponseEntity>(sql0, new { TagId = TagId });
                    tag0 = _tag0.FirstOrDefault();

                    if (tag0 == null)
                    {
                        throw new Exception("Data not found");
                    }

                    var _tag1 = connection.Query<Tag1ResponseEntity>(sql1, new { TagId = TagId });
                    tag1 = _tag1.ToList();

                    tag0.Posts = new List<Tag1ResponseEntity>();
                    tag0.Posts.AddRange(tag1);

                    return tag0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<int> Insert(List<Tag0ParameterEntity> Tag)
        {
            try
            {
                List<int> tagId = new List<int>();

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using (var transaction = new TransactionScope())
                    {
                        string sql0 = @"INSERT INTO [Tag0] VALUES 
                                (@Label)
                                SELECT SCOPE_IDENTITY()";
                        string sql1 = @"INSERT INTO [Post1] VALUES 
                                (@PostId, @TagId)
                                ";

                        try
                        {
                            foreach (Tag0ParameterEntity x in Tag)
                            {
                                var _id = connection.Query<int>(sql0, x);
                                tagId.Add(_id.First());

                                if (x.Posts.Count > 0)
                                {
                                    List<Post1Entity> post1 = new List<Post1Entity>();

                                    foreach (Tag1ParameterEntity y in x.Posts)
                                    {
                                        post1.Add(new Post1Entity
                                        {
                                            PostId = y.PostId,
                                            TagId = _id.First(),
                                        });
                                    }

                                    connection.Execute(sql1, post1);
                                }
                            }

                            transaction.Complete();
                        }
                        catch (Exception ex)
                        {
                            transaction.Dispose();
                            throw ex;
                        }
                    }

                    return tagId;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Update(Tag0ParameterEntity Tag, int TagId)
        {
            try
            {
                Tag0ResponseEntity tag0 = Get(TagId);
                List<int> _addPosts = new List<int>();
                List<int> _deletePosts = new List<int>();
                List<int> oldPosts = new List<int>();
                List<int> newPosts = new List<int>();

                tag0.Posts.ForEach(x => oldPosts.Add(x.PostId));
                Tag.Posts.ForEach(x => newPosts.Add(x.PostId));

                _addPosts = newPosts.Except(oldPosts).ToList();
                _deletePosts = oldPosts.Except(newPosts).ToList();

                List<Post1Entity> addPosts = new List<Post1Entity>();
                List<Post1Entity> deletePosts = new List<Post1Entity>();

                _addPosts.ForEach(x => addPosts.Add(new Post1Entity { TagId = TagId, PostId = x }));
                _deletePosts.ForEach(x => deletePosts.Add(new Post1Entity { TagId = TagId, PostId = x }));

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {

                    using (var transaction = new TransactionScope())
                    {
                        string sql0 = @"UPDATE [Tag0] SET 
                                Label = @Label
                                WHERE TagId = @TagId
                                ";
                        string sql1 = @"INSERT INTO [Post1] VALUES 
                                (@PostId, @TagId)
                                ";
                        string sql2 = @"DELETE [Post1] WHERE
                                PostId = @PostId AND TagId = @TagId
                                ";

                        try
                        {
                            connection.Execute(sql0, new { TagId = TagId, Label = Tag.Label });
                            connection.Execute(sql1, addPosts);
                            connection.Execute(sql2, deletePosts);

                            transaction.Complete();
                        }
                        catch (Exception ex)
                        {
                            transaction.Dispose();
                            throw ex;
                        }
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
