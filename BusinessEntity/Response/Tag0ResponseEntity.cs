﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BasicAPI.BusinessEntity
{
    public class Tag0ResponseEntity
    {
        public int TagId { get; set; }
        public string Label { get; set; }
        public List<Tag1ResponseEntity> Posts { get; set; }
    }
}
