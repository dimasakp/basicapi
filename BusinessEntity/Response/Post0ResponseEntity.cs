﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BasicAPI.BusinessEntity
{
    public class Post0ResponseEntity
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public List<Post1ResponseEntity> Tags { get; set; }
    }
}
