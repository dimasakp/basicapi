﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BasicAPI.BusinessEntity
{
    public class Tag1ResponseEntity
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
