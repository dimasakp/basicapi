﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BasicAPI.BusinessEntity.Parameter
{
    public class Tag0ParameterEntity
    {
        public string Label { get; set; }
        public List<Tag1ParameterEntity> Posts { get; set; }
    }
}
