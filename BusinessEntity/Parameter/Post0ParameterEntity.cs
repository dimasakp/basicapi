﻿using BasicAPI.BusinessEntity.Parameter;
using System;
using System.Collections.Generic;
using System.Text;

namespace BasicAPI.BusinessEntity
{
    public class Post0ParameterEntity
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public List<Post1ParameterEntity> Tags { get; set; }
    }
}
