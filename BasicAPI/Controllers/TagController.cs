﻿using BasicAPI.BusinessEntity;
using BasicAPI.BusinessEntity.Parameter;
using BasicAPI.BusinessService.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasicAPI.Controllers
{
    [Route("api/[controller]")]
    public class TagController : ControllerBase
    {
        #region Service Variable

        private readonly ITagService _tagService;

        #endregion

        #region Constructor

        public TagController(ITagService tagService)
        {
            _tagService = tagService;
        }

        #endregion

        [HttpGet]
        public async Task<ActionResult<List<Tag0ResponseEntity>>> Get()
        {
            try
            {
                List<Tag0ResponseEntity> Result = new List<Tag0ResponseEntity>();
                Result = _tagService.Get();

                return Ok(new { Data = Result });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Error = ex.Message });
            }
        }

        [HttpGet("{TagId}")]
        public async Task<ActionResult<Tag0ResponseEntity>> Get(int TagId)
        {
            try
            {
                Tag0ResponseEntity Result = new Tag0ResponseEntity();
                Result = _tagService.Get(TagId);

                return Ok(new { Data = Result });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Error = ex.Message });
            }
        }

        [HttpPost]
        public async Task<ActionResult> Insert([FromBody] List<Tag0ParameterEntity> Tag)
        {
            try
            {
                #region Validation

                string errMsg = "";

                if (Tag.Count == 0)
                {
                    errMsg += "Request body can't be empty. ";
                }
                else
                {
                    foreach (var x in Tag)
                    {
                        if (string.IsNullOrEmpty(x.Label))
                        {
                            errMsg += "Parameter Label can't be empty. ";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(errMsg))
                {
                    throw new Exception(errMsg);
                }

                #endregion

                List<int> res = _tagService.Insert(Tag);
                string msg = res.Count > 1 ? "Insert Tags success" : "Insert Tag success";
                return Ok(new { Message = msg, TagId = res  });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Error = ex.Message });
            }
        }

        [HttpPost("{TagId}")]
        public async Task<ActionResult> Update([FromBody] Tag0ParameterEntity Tag, int TagId)
        {
            try
            {
                #region Validation

                string errMsg = "";

                if (Tag == null)
                {
                    errMsg += "Request body can't be empty. ";
                }
                else
                {
                    if (string.IsNullOrEmpty(Tag.Label))
                    {
                        errMsg += "Parameter Label can't be empty. ";
                    }
                }

                if (!string.IsNullOrEmpty(errMsg))
                {
                    throw new Exception(errMsg);
                }

                #endregion

                _tagService.Update(Tag, TagId);

                return Ok(new { Message = "Update Tag success" });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Error = ex.Message });
            }
        }

        [HttpDelete("{TagId}")]
        public async Task<ActionResult> Delete(int TagId)
        {
            try
            {
                _tagService.Delete(TagId);

                return Ok(new { Message = "Delete Tag success" });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Error = ex.Message });
            }
        }
    }
}
