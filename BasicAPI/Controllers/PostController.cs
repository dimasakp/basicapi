﻿using BasicAPI.BusinessEntity;
using BasicAPI.BusinessService.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BasicAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        #region Service Variable

        private readonly IPostService _postService;

        #endregion

        #region Constructor

        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        #endregion

        [HttpGet]
        public async Task<ActionResult<List<Post0ResponseEntity>>> Get()
        {
            try
            {
                List<Post0ResponseEntity> Result = new List<Post0ResponseEntity>();
                Result = _postService.Get();

                return Ok(new { Data = Result });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Error = ex.Message });
            }
        }

        [HttpGet("{PostId}")]
        public async Task<ActionResult<Post0ResponseEntity>> Get(int PostId)
        {
            try
            {
                Post0ResponseEntity Result = new Post0ResponseEntity();
                Result = _postService.Get(PostId);

                return Ok(new { Data = Result });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Error = ex.Message });
            }
        }

        [HttpPost]
        public async Task<ActionResult> Insert(Post0ParameterEntity Post)
        {
            try
            {
                #region Validation

                string errMsg = "";

                if (Post == null)
                {
                    errMsg += "Request body can't be empty. ";
                }
                else
                {
                    if (string.IsNullOrEmpty(Post.Title))
                    {
                        errMsg += "Parameter Title can't be empty. ";
                    }
                    if (string.IsNullOrEmpty(Post.Content))
                    {
                        errMsg += "Parameter Content can't be empty. ";
                    }
                }

                if (!string.IsNullOrEmpty(errMsg))
                {
                    throw new Exception(errMsg);
                }

                #endregion

                int res = _postService.Insert(Post);

                return Ok(new { Message = "Insert Post success", PostId = res });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Error = ex.Message });
            }
        }

        [HttpPost("{PostId}")]
        public async Task<ActionResult> Update(Post0ParameterEntity Post, int PostId)
        {
            try
            {
                #region Validation

                string errMsg = "";

                if (Post == null)
                {
                    errMsg += "Request body can't be empty. ";
                }
                else
                {
                    if (string.IsNullOrEmpty(Post.Title))
                    {
                        errMsg += "Parameter Title can't be empty. ";
                    }
                    if (string.IsNullOrEmpty(Post.Content))
                    {
                        errMsg += "Parameter Content can't be empty. ";
                    }
                }

                if (!string.IsNullOrEmpty(errMsg))
                {
                    throw new Exception(errMsg);
                }

                #endregion

                _postService.Update(Post, PostId);

                return Ok(new { Message = "Update Post success" });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Error = ex.Message });
            }
        }

        [HttpDelete("{PostId}")]
        public async Task<ActionResult> Delete(int PostId)
        {
            try
            {
                _postService.Delete(PostId);

                return Ok(new { Message = "Delete Post success" });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Error = ex.Message });
            }
        }
    }
}
