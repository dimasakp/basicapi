﻿using BasicAPI.BusinessEntity;
using BasicAPI.BusinessService.Interface;
using BasicAPI.DataRepository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace BasicAPI.BusinessService
{
    public class PostService : IPostService
    {
        #region Variables

        private readonly IPostRepository _postRepository;

        #endregion

        #region Constructor

        public PostService(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }

        #endregion

        public bool Delete(int PostId)
        {
            try
            {
                return _postRepository.Delete(PostId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Post0ResponseEntity> Get()
        {
            try
            {
                return _postRepository.Get();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Post0ResponseEntity Get(int PostId)
        {
            try
            {
                return _postRepository.Get(PostId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Insert(Post0ParameterEntity Post)
        {
            try
            {
                return _postRepository.Insert(Post);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Update(Post0ParameterEntity Post, int PostId)
        {
            try
            {
                return _postRepository.Update(Post, PostId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
