﻿using BasicAPI.BusinessEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace BasicAPI.BusinessService.Interface
{
    public interface IPostService
    {
        public List<Post0ResponseEntity> Get();
        public Post0ResponseEntity Get(int PostId);
        public int Insert(Post0ParameterEntity Post);
        public bool Update(Post0ParameterEntity Post, int PostId);
        public bool Delete(int PostId);
    }
}
