﻿using BasicAPI.BusinessEntity;
using BasicAPI.BusinessEntity.Parameter;
using BasicAPI.BusinessService.Interface;
using BasicAPI.DataRepository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace BasicAPI.BusinessService
{
    public class TagService : ITagService
    {
        #region Variables

        private readonly ITagRepository _tagRepository;

        #endregion

        #region Constructor

        public TagService(ITagRepository tagRepository)
        {
            _tagRepository = tagRepository;
        }

        #endregion

        public bool Delete(int TagId)
        {
            try
            {
                return _tagRepository.Delete(TagId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Tag0ResponseEntity> Get()
        {
            try
            {
                return _tagRepository.Get();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Tag0ResponseEntity Get(int TagId)
        {
            try
            {
                return _tagRepository.Get(TagId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<int> Insert(List<Tag0ParameterEntity> Tag)
        {
            try
            {
                return _tagRepository.Insert(Tag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Update(Tag0ParameterEntity Tag, int TagId)
        {
            try
            {
                return _tagRepository.Update(Tag, TagId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
